# Contributing

If you want to add a new base image to this project, please do the following:  
1. Clone the repository to your local machine and checkout a new branch from `master`.
1. Create a new file with the following naming convention: `Dockerfile.<IMAGE_NAME>:<IMAGE_TAG>`. Example: `Dockerfile.couchbase:enterprise-6.0.2`.
1. Add instructions to the Dockerfile following [best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/).
1. Test image by building it locally.
    ```sh
    docker build --file Dockerfile.<IMAGE_NAME>:<IMAGE_TAG> .
    ```
1. Add new job to `.gitlab-ci.yml` that extends the `.build` job and only runs when that specific Dockerfile changes. The job name must match the suffix to the Dockerfile that you just created. For example, if you created `Dockerfile.couchbase:enterprise-6.0.2`, then your CI job would look like this:
    ```yaml
    couchbase:enterprise-6.0.2:
      extends: .build
      only:
        changes:
        - Dockerfile.couchbase:enterprise-6.0.2
    ```
1. Commit (with a detailed message) and push your changes to GitLab.
1. Issue a Merge Request to merge your branch into `master`.
1. Once approved, verify that the pipeline triggered by the merge was successful.