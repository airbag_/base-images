# base-images

Container images with some modifications that serve as a starting point for most applications.

## Usage

The address of the container registry tied to this project is
`registry.gitlab.com/jckknfd_jggrnt/base-images`. The image name and tag of the image you want
to use is the suffix of its corresponding Dockerfile name.

For example, if you want to use the image built from `Dockerfile.nginx:1.16.0-alpine`, you would specify the image as `registry.gitlab.com/jckknfd_jggrnt/base-images/nginx:1.16.0-alpine`.